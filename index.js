//Define usage
if (process.argv.length < 3) {
  console.log('Usage: node ' + process.argv[1] + ' FILENAME');
  process.exit(1);
}

//Define file name
var fs = require('fs')
  , filename = process.argv[2];

//Read data from file and begin parse and search process
fs.readFile(filename, 'utf8', function(err, data) {
  if (err) throw err;
  parseAndSearch(data);
});

function parseAndSearch(data){
  inputArr = data.split('\n'); //Split data into array by return character

  //Remove empty elements, if any
  for(let itm in inputArr){
    if(inputArr[itm] === ""){
      inputArr.splice(itm,1);
    }
  }

  //**Add addtional objects for tracking other components if needed. 
  tstatAllerts = {}; //Object to store array of 3 time stamps when tstat condition is met
  battAllerts = {}; //Object to store array of 3 time stamps when batt condition is met

  output = [];

  //Itterate through dataset and take action 
  for(let line of inputArr){
    //Parse data
    data = line.split('|');
    date = parseDate(data[0]);
    satellite = data[1];
    red_high = parseFloat(data[2]);
    red_low = parseFloat(data[5]);
    value = parseFloat(data[6]);
    component = data[7]

    //**Logic for checking specific telemetry components. Add more if needed
    //Check if component value is lower than Red Low
    if(value<red_low && component == 'BATT'){ 
      battAllerts = updateAllertTracker(battAllerts, satellite, date); //Update battery object
      output = updateOutput(battAllerts, satellite, component, date, output, 'RED LOW'); //Update output
    }
    //Check if value is higher than Red High
    if(value>red_high && component == 'TSTAT'){
      tstatAllerts = updateAllertTracker(tstatAllerts, satellite, date); //Update tstat object
      output = updateOutput(tstatAllerts, satellite, component, date, output, 'RED HIGH'); //Update output
    }
  }
  console.log(output); //Print output
}

function updateAllertTracker(Allerts, satellite, date){
  //Add new property based on satellite if it doesn't already exist
  if(!Allerts[satellite]){
    Allerts[satellite] = [date]
  }else{
    Allerts[satellite].unshift(date) //Add new timestamp to start of array
  }
  return Allerts; //Return object
}

function updateOutput(Allerts, satellite, component, date, output, message){
  //Three or more events exist, check if output needs to be updated
  if(Allerts[satellite].length >= 3){
    Allerts[satellite].length = 3;
    tOldest = Allerts[satellite][2];
    tNewest = Allerts[satellite][0]
    //Check if the difference between oldest and newest event is more than 5 minutes
    if((tNewest-tOldest)/60000<=5){
      newEvent = {};
      //Create new object for output
      newEvent = {
        satelliteId: parseInt(satellite),
        severity: message,
        component: component,
        timestamp: formatDate(date)
      };
      output.push(newEvent); //Add to output array
    }
  }

  return output;
}

//Create new string to add to output object
function formatDate(date){
  return(
    date.getFullYear()
    +"-"+ ("0"+(date.getMonth()+1)).slice(-2)
    +"-"+ ("0"+date.getDate()).slice(-2)
    +"T"+ ("0"+date.getHours()).slice(-2)
    +":"+ ("0"+date.getMinutes()).slice(-2)
    +":"+ ("0"+date.getSeconds()).slice(-2)
    +"."+ ("0"+date.getMilliseconds()).slice(-3) +"Z"
  );
}

//Parse date string into new javascript Date object 
function parseDate(d){
  return new Date(
    parseInt(d.slice(0,4)), //year
    parseInt(d.slice(4,6))-1, //month
    parseInt(d.slice(6,8)), //day
    parseInt(d.slice(9,11)), //hour
    parseInt(d.slice(12,14)), //minutes
    parseInt(d.slice(15,17)), //seconds
    parseInt(d.slice(18)) //milliseconds
  );  
}

